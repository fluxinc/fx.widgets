# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

input_change = (el) ->
  $form = $(el).closest('form.task-form')
  recalculate_form($form)
  recalculate_totals()

recalculate_form = (form) ->
  console.log form
  old_duration = $('[name="old_duration"]', form)[0].value
  rate = $('[name="rate"]', form)[0].value
  console.log old_duration
  $('[name="old_cost"]', form).val ( old_duration / 60 * rate).toFixed(2)
  new_duration = $('[name="new_duration"]', form)[0].value
  $('[name="new_cost"]', form).val ( new_duration / 60 * rate).toFixed(2)

recalculate_all = ->
  $('form.task-form').each ->
    recalculate_form(this)
  recalculate_totals()

total_of_field = (name) ->
  total = 0
  $('[name="'+name+'"]').each ->
    total += parseFloat $(this).val()
  total

recalculate_totals = ->
  form = $('form.totals-form')[0]
  exam_cost = total_of_field('old_cost')
  new_exam_cost = total_of_field('new_cost')
  exam_time = total_of_field('old_duration')
  new_exam_time = total_of_field('new_duration')

  device_count =  parseInt( $('[name="device_count"]', form)[0].value, 10 )

  scans_per_day = parseInt( $('[name="scans_per_day"]', form)[0].value, 10 ) * device_count
  scans_per_year = scans_per_day * 250
  scans_per_week = scans_per_year / 52


  day_time = exam_time * scans_per_day
  week_time = exam_time * scans_per_week
  year_time = exam_time * scans_per_year

  exam_time_saved = exam_time - new_exam_time
  day_time_saved = exam_time_saved * scans_per_day
  week_time_saved = exam_time_saved * scans_per_week
  year_time_saved = exam_time_saved * scans_per_year

  exam_cost_saved = exam_cost - new_exam_cost
  day_cost_saved = exam_cost_saved * scans_per_day
  week_cost_saved = exam_cost_saved * scans_per_week
  year_cost_saved = exam_cost_saved * scans_per_year

  $('#exam_time').val exam_time
  $('#day_time').val day_time
  $('#day_time_saved').val day_time_saved
  $('#year_time_saved').val Math.round(year_time_saved / 60)
  $('#day_cost_saved').val Math.round(day_cost_saved)
  $('#year_cost_saved').val Math.round(year_cost_saved)

$ ->
  $('form.task-form').on "change", 'input', ->
    input_change(this)

  $('form.totals-form').on "change", 'input', ->
    recalculate_totals()

  $('body').on "click", "remove-row", ->
    $(this).closest('div.task-row').remove()
    recalculate_totals()

  recalculate_all()
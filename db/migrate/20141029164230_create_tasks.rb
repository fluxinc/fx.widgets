class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :name
      t.integer :default_rate
      t.integer :old_duration
      t.integer :new_duration
      t.boolean :is_affected

      t.timestamps
    end
  end
end

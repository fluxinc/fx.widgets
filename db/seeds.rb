# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Task.delete_all

Task.create( name: 'Filling in Worksheet', default_rate: 30, is_affected: true, old_duration: 5, new_duration: 5 )
Task.create( name: 'Acquiring and storing study', default_rate: 30, is_affected: true, old_duration: 25, new_duration: 25 )
Task.create( name: 'Scanning worksheet', default_rate: 30, is_affected: true, old_duration: 1, new_duration: 0 )
Task.create( name: 'Transposing worksheet', default_rate: 30, is_affected: true, old_duration: 2, new_duration: 0 )
Task.create( name: 'Copying measurements', default_rate: 30, is_affected: true, old_duration: 5, new_duration: 0 )
